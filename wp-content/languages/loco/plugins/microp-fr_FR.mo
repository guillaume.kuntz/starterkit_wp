��    #      4      L      L     M      j     �     �  	   �     �     �  B   �  
        "  
   '     2     F     O     \  	   q     {     �     �     �  
   �     �     �     �     �          	     %     6     J  '   e  	   �     �     �  p  �  &     +   ;     g  
   s     ~     �     �  Z   �  
     	        %     ;  
   [     f     ~     �  )   �     �     �     �     �     	     	     1	     N	     h	     o	     �	     �	     �	  +   �	  	   �	     �	     
   10 (very low, smallest file) 100 (best quality, biggest file) 30 (low) 50 (average) 60 (good) 70 (high quality) 80 (very high quality) An Error has occured. Please try again or contact plugin's author. Compatible Crop Crop Image Crop featured image Crop it! Custom Label Default JPEG Quality Full Size Generate Retina/HiDPI (@2x): Hard crop mode Large Medium New image: No Original picture dimensions: Pick the image size: Previous image: Size Soft proportional crop mode Source too small Target JPEG Quality Target picture dimensions: The image has been cropped successfully Thumbnail Visible Yes Project-Id-Version: mic
POT-Creation-Date: 2015-05-01 00:50+0100
PO-Revision-Date: 2019-05-23 12:40+0000
Last-Translator: guillaume.kuntz <guillaume.kuntz.simplon@gmail.com>
Language-Team: Français
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e
X-Poedit-Basepath: .
Language: fr_FR
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: C:\wamp\www\wordpress\wp-content\plugins\manual-image-crop
Report-Msgid-Bugs-To: 
Plural-Forms: nplurals=2; plural=n > 1;
X-Loco-Version: 2.2.2; wp-5.2.1 10 (basse qualité, compression forte) 100 (qualité maximale, compression faible) 30 (faible) 50 (moyen) 60 (bon) 70 (haute qualité) 80 (très haute qualité) Une erreur est survenue. Merci de réessayer plus tard ou de contacter l'auteur du plugin. Compatible Recadrage Recadrer l&apos;image Recadrer l&apos;image à la Une Recadrer ! Légende personnalisée Qualité JPEG par défaut Taille d'origine Générer l&apos;image Retina/HiDPI (@2x) Recadrage non proportionnel Large Moyen Nouvelle image&nbsp;: Non Dimension originales&nbsp;: Choisissez une taille&nbsp;: Image précédente&nbsp;: Taille Recadrage proportionnel Image source trop petite Qualité JPEG Dimensions cibles&nbsp;: L&apos;image a été correctement recadrée Miniature Visible Oui 