<?php
/**
 * Plugin Name: Go to top
 * Plugin URI:  https://example.com/plugin-name
 * Description: bin Go to top
 * Version:     1.0.0
 * Author:      Guillaume Kuntz
 * Author URI:  https://example.com
 * Text Domain: gtt
 * License:     GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 */

function gtt_footer(){
?>
    <a
        href="#"
        class="gtt-go-to-top"
    >
        <?php esc_html_e("Go to top", "gtt"); ?>
    </a>
<?php
}

add_action('wp_footer', 'gtt_footer', 10000);

function gtt_plugins_loaded() {
    $plugin_rel_path = basename( dirname( __FILE__ ) ) . '/assets/lang';
    load_plugin_textdomain( 'gtt', false, $plugin_rel_path );
}

add_action('plugins_loaded', 'gtt_plugins_loaded');
