<?php

// my first shortcode

/**
* fonction shortcode permet de rendre un button
* [my-first-button title="Mon title" link="http://exemple.com" class="ma-classe" target="_blank"]
*/
function simplon_my_first_button($param, $content){
   shortcode_atts(
     array(
       'title' => __('Go to' , 'simplon'),
       'link' => '',
       'class' => '',
       'target' => '',
     ),
     $param
   );

// cree une string et la stocke
   ob_start();

?>
<!-- creation du lien du button -->

        <a
            href="<?php echo esc_url($param['link']); ?>"
            class="<?php echo esc_attr($param['class']); ?>"
            target="<?php echo esc_attr($param['target']); ?>"
        >
            <?php echo esc_html($param['title']); ?>
        </a>

<?php

// return ma string de ob_strat
   return ob_get_clean();

}
    add_shortcode('my-first-button', 'simplon_my_first_button');
