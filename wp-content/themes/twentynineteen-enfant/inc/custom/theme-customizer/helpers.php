<?php 

/**
 * Add a post State
 *
 * @param   WP_Post  $post  
 * @param   string   $option 
 * @param   string   $title
 *
 * @return  string
 */
function simplon_add_post_state ( $post, $option, $title ) {
	
	$page_id   = (int) get_theme_mod( $option );

	if( function_exists( 'pll_get_post_language' ) && function_exists( 'pll_get_post' ) ) {

		$page_lang   = pll_get_post_language( $page_id );
		$post_lang   = pll_get_post_language( (int) $post->ID );
		$pll_post_id = (int) pll_get_post( (int) $post->ID, $page_lang );

		if ( $pll_post_id === $page_id ) {
			return $title;
		}
		
	}
	else {

		if ( (int) $post->ID === $page_id ) {
			return $title;
		}

	}

	return '';

}