<?php 

/**
 * Enqueue customizer scripts
 *
 * @return  void
 */
function simplon_customize_controls_scripts() {

	wp_enqueue_script( 
		'theme-customize-controls', 
		get_stylesheet_directory_uri() . '/inc/custom/theme-customizer/assets/customize-controls.js', 
		array(), 
		'1.0', 
		true 
	);

}
add_action( 'customize_controls_enqueue_scripts', 'simplon_customize_controls_scripts' );

/**
 * Enqueue customizer styles
 *
 * @return  void
 */
function simplon_customize_controls_styles() {

	wp_enqueue_style( 
		'theme-customize-controls', 
		get_stylesheet_directory_uri() . '/inc/custom/theme-customizer/assets/customize-controls.css', 
		array(), 
		'1.0' 
	);

}
add_action( 'customize_controls_print_styles', 'simplon_customize_controls_styles' );
