<?php 

/**
 * Assign a Template to a page
 *
 * @return  void
 */
function simplon_assign_template_to_page() {
	$tpl = $_POST['tpl'];
	$id  = intval( $_POST['id'] );

	if ( empty( $tpl ) || empty( $id ) ) {
		wp_die();
	}

	echo update_post_meta( $id, '_wp_page_template', $tpl );

	wp_die(); // this is required to terminate immediately and return a proper response
}
add_action( 'wp_ajax_simplon_assign_template_to_page', 'simplon_assign_template_to_page' );
