<?php

/**
 * Custom Dropdown Pages
 */
class SIMPLON_Dropdown_Pages_Control extends WP_Customize_Control {

	/**
	 * Template assigned to the Page.
	 *
	 * @since 3.4.0
	 * @var string
	 */
	public $template = '';

	/**
	 * Type of the control.
	 *
	 * @since 3.4.0
	 * @var string
	 */
	public $type = 'dropdown-pages';
 
	public function render_content() {
		
		$input_id         = '_customize-input-' . $this->id;
		$description_id   = '_customize-description-' . $this->id;
		$describedby_attr = ( ! empty( $this->description ) ) ? ' aria-describedby="' . esc_attr( $description_id ) . '" ' : '';
		$classname        = 'simplon-dropdown-pages';

		if ( ! empty( $this->label ) ) : 
			?>
				<label for="<?php echo esc_attr( $input_id ); ?>" class="customize-control-title"><?php echo esc_html( $this->label ); ?></label>
			<?php 
		endif; 
		
		if ( ! empty( $this->description ) ) : 
			?>
				<span id="<?php echo esc_attr( $description_id ); ?>" class="description customize-control-description"><?php echo $this->description; ?></span>
			<?php 
		endif; 

		$dropdown_name     = '_customize-dropdown-pages-' . $this->id;
		$show_option_none  = __( '&mdash; Select &mdash;' );
		$option_none_value = '0';
		$dropdown          = wp_dropdown_pages(
			array(
				'name'              => $dropdown_name,
				'echo'              => 0,
				'show_option_none'  => $show_option_none,
				'option_none_value' => $option_none_value,
				'selected'          => $this->value(),
				'class'             => $classname,
			)
		);

		if ( empty( $dropdown ) ) {
			$dropdown = sprintf( '<select id="%1$s" name="%1$s" class="%2$s">', esc_attr( $dropdown_name ), $classname );
			$dropdown .= sprintf( '<option value="%1$s">%2$s</option>', esc_attr( $option_none_value ), esc_html( $show_option_none ) );
			$dropdown .= '</select>';
		}

		// Hackily add in the data link parameter.
		$dropdown = str_replace( '<select', '<select ' . $this->get_link() . ' id="' . esc_attr( $input_id ) . '" ' . $describedby_attr . ' data-template="' . $this->template . '"', $dropdown );

		// Even more hacikly add auto-draft page stubs.
		// @todo Eventually this should be removed in favor of the pages being injected into the underlying get_pages() call. See <https://github.com/xwp/wp-customize-posts/pull/250>.
		$nav_menus_created_posts_setting = $this->manager->get_setting( 'nav_menus_created_posts' );
		if ( $nav_menus_created_posts_setting && current_user_can( 'publish_pages' ) ) {
			$auto_draft_page_options = '';
			foreach ( $nav_menus_created_posts_setting->value() as $auto_draft_page_id ) {
				$post = get_post( $auto_draft_page_id );
				if ( $post && 'page' === $post->post_type ) {
					$auto_draft_page_options .= sprintf( '<option value="%1$s">%2$s</option>', esc_attr( $post->ID ), esc_html( $post->post_title ) );
				}
			}
			if ( $auto_draft_page_options ) {
				$dropdown = str_replace( '</select>', $auto_draft_page_options . '</select>', $dropdown );
			}
		}

		echo $dropdown;

		if ( $this->allow_addition && current_user_can( 'publish_pages' ) && current_user_can( 'edit_theme_options' ) ) : // Currently tied to menus functionality. 
			?>
				<button type="button" class="button-link add-new-toggle">
					<?php
					/* translators: %s: add new page label */
					printf( __( '+ %s' ), get_post_type_object( 'page' )->labels->add_new_item );
					?>
				</button>
				<div class="new-content-item">
					<label for="create-input-<?php echo $this->id; ?>"><span class="screen-reader-text"><?php _e( 'New page title' ); ?></span></label>
					<input type="text" id="create-input-<?php echo $this->id; ?>" class="create-item-input" placeholder="<?php esc_attr_e( 'New page title&hellip;' ); ?>">
					<button type="button" class="button add-content"><?php _e( 'Add' ); ?></button>
				</div>
			<?php 
		endif; 

	}

	/**
	 * Enqueue control related scripts/styles.
	 *
	 * @since 3.4.0
	 */
	public function enqueue() {

		wp_enqueue_script( 
			'simplon-dropdown-pages', 
			get_stylesheet_directory_uri() . '/inc/custom/theme-customizer/assets/dropdown-pages.js', 
			array(), 
			'1.0', 
			true 
		);
	}

}

