<?php

/**
 * WYSIWYG Control
 */
class SIMPLON_Wysiwyg_Control extends WP_Customize_Control {

	/**
	 * Type of the control.
	 *
	 * @since 3.4.0
	 * @var string
	 */
	public $type = 'editor';

	/**
	 * Constructor.
	 *
	 * Supplied `$args` override class property defaults.
	 *
	 * If `$args['settings']` is not defined, use the $id as the setting ID.
	 *
	 * @since 3.4.0
	 *
	 * @param WP_Customize_Manager $manager Customizer bootstrap instance.
	 * @param string               $id      Control ID.
	 * @param array                $args    Optional. Arguments to override class property defaults.
	 */
	public function __construct( $manager, $id, $args = array() ) {
		parent::__construct( $manager, $id, $args );
		
		$this->input_attrs['data-editor'] = empty( $args['editor_settings'] ) ? 
			wp_json_encode( $this->get_settings() ) : 
			wp_json_encode( $args['editor_settings'] )
		;
	}

	/**
	 * Render the content on the theme customizer page
	 */
	public function render_content() {
		
		?>
			<label>
				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				<textarea class="simplon-wysiwyg-selector" id="simplon-wysiwyg-selector-<?php echo intval( $this->instance_number ); ?>" <?php $this->input_attrs(); ?> <?php $this->link(); ?>><?php echo esc_textarea( $this->value() ); ?></textarea>
			</label>
		<?php

	}

	/**
	 * Enqueue control related scripts/styles.
	 *
	 * @since 3.4.0
	 */
	public function enqueue() {

		wp_enqueue_editor();
		
		wp_enqueue_script( 
			'simplon-wysiwyg', 
			get_stylesheet_directory_uri() . '/inc/custom/theme-customizer/assets/wysiwyg.js', 
			array(), 
			'1.0', 
			true 
		);
	}

	/**
	 * Get WYSIWYG Settings
	 *
	 * @return  array
	 */
	public function get_settings() {
		return array(
			'mediaButtons' => true,
			'tinymce'      => array (
				'menubar'  => true,
				'wpautop'  => true,			
				'toolbar1' => implode( ',', apply_filters( 'mce_buttons', array(), 'editor' ) ),
				'toolbar2' => implode( ',', apply_filters( 'mce_buttons_2', array(), 'editor' ) ),
				'toolbar3' => implode( ',', apply_filters( 'mce_buttons_3', array(), 'editor' ) ),
				'toolbar4' => implode( ',', apply_filters( 'mce_buttons_4', array(), 'editor' ) ),
				'plugins'  => implode( ',', apply_filters( 'tiny_mce_plugins', array(
					'advlist',
					'charmap',
					'colorpicker',
					'compat3x',
					'directionality',
					'fullscreen',
					'hr',
					'image',
					'lists',
					'media',
					'paste',
					'tabfocus',
					'table',
					'textcolor',
					'wordpress',
					'wpautoresize',
					'wpdialogs',
					'wpeditimage',
					'wpemoji',
					'wpgallery',
					'wplink',
					'wptextpattern',
					'wpview',
				) ) ),
			),
		);
	}
}