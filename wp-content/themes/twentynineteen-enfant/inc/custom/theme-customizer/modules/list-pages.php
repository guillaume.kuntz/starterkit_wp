<?php

if ( class_exists( 'WP_Customize_Control' ) ) {

	class SIMPLON_List_Pages_Control extends WP_Customize_Control {

		/**
		 * Template assigned to the Page.
		 *
		 * @since 3.4.0
		 * @var string
		 */
		public $template = '';

		/**
		 * Type of the control.
		 *
		 * @since 3.4.0
		 * @var string
		 */
		public $type = 'select';
	 
		public function render_content() {

			$pages = new WP_Query( array (
				'post_type'      => array(
					'page',
				),
				'posts_per_page' => -1,
				'orderby'        => 'title',
				'order'          => 'ASC',
			) );

			?>
			<label>
				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				<?php

					if ( $pages->have_posts() ) :

						?>
							<select class="simplon-page-selector" data-template="<?php echo $this->template; ?>" <?php $this->link(); ?>>
								
								<option value="0">
									<?php _e( 'Chose a Page', 'simplon' ); ?>
								</option>

								<?php

									while ( $pages->have_posts() ) :  $pages->the_post();
										
										?>
											<option value="<?php the_ID(); ?>" <?php selected ( $this->value(), get_the_ID(), true ); ?>>
												<?php the_title(); ?>
											</option>
										<?php

									endwhile;

								?>


							</select>
						<?php


					endif;

					wp_reset_postdata();

				?>
			</label>
			<?php

		}

		/**
		 * Enqueue control related scripts/styles.
		 *
		 * @since 3.4.0
		 */
		public function enqueue() {

			wp_enqueue_script( 
				'simplon-list-pages', 
				get_stylesheet_directory_uri() . '/inc/custom/theme-customizer/assets/list-pages.js', 
				array(), 
				'1.0', 
				true 
			);
		}

	}

}

/**
 * Assign a Template to a page
 *
 * @return  void
 */
function simplon_assign_template_to_page() {
	$tpl = $_POST['tpl'];
	$id  = intval( $_POST['id'] );

	if ( empty( $tpl ) || empty( $id ) ) {
		wp_die();
	}

	echo update_post_meta( $id, '_wp_page_template', $tpl );

	wp_die(); // this is required to terminate immediately and return a proper response
}
add_action( 'wp_ajax_simplon_assign_template_to_page', 'simplon_assign_template_to_page' );

