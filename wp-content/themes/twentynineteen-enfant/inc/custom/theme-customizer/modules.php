<?php

// Ajax 
include 'modules/ajax.php';

// Panels 
if ( class_exists( 'WP_Customize_Panel' ) ) {
	include 'modules/pannel.php';
}

// Controls
if ( class_exists( 'WP_Customize_Control' ) ) {
	include 'modules/dropdown-pages.php';
	include 'modules/wysiwyg.php';
}
