
( function ( $ ) {

	$( function () {

		var $selectors = $( '.simplon-dropdown-pages' );

		$( '#save' ).on( 'click', function(e) {

			$selectors.each( function() {
				var $this = $( this ),
					tpl   = $this.data( 'template' ),
					id    = parseInt( $this.val() )
				;
				
				if ( tpl !== '' && id !== 0 ) {
					$.ajax( {
						type: 'POST',
						url : ajaxurl,
						data: {
							'action': 'simplon_assign_template_to_page',
							'tpl'   : tpl,
							'id'    : id,
						},
						success: function ( result ) {
							console.log( result );
						}
					} );
				}
			} );

		} );

	} );

} ) ( jQuery );