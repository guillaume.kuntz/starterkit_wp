
( function ( $ ) {

	$( function () {

		var $selectors = $( '.mouvement-wysiwyg-selector' );

		$( window ).on( 'load', function() {

			$selectors.each( function() {
				var $textarea = $( this ),
					id        = $textarea.attr( 'id' ),
					options   = $textarea.data( 'editor' ),
					defaults  = wp.editor.getDefaultSettings(),
					settings  = $.extend( true, {}, defaults, options )
				;

				wp.editor.initialize( id, settings );
			} );

			setInterval( function() {

				$selectors.each( function() {
					var $textarea = $( this ),
						id        = $textarea.attr( 'id' ),
						oldValue  = $textarea.val(),
						newValue  = wp.editor.getContent( id )
					;

					if ( oldValue == newValue ) {
						return;
					}

					$textarea.val( newValue );
					$textarea.trigger( 'change' );
				} );

			}, 500 );

		} );
	} );

} ) ( jQuery );