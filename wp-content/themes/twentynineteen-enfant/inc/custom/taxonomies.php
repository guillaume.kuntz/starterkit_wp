<?php

/**
 * Add new taxonomies
 *
 * @return  void
 */
function simplon_taxonomies() {

	// create Skill taxonomy
	register_taxonomy(
		'skill',
		array ( 'page', 'project' ),
		array(
			'label'        => __( 'Skill', 'simplon' ),
			'rewrite'      => array( 'slug' => 'skill' ),
			'capabilities' => array(
				'assign_terms' => 'edit_posts',
				'edit_terms'   => 'manage_categories',
				'manage_terms' => 'manage_categories',
				'delete_terms' => 'manage_categories',
			)
		)
	);

}
add_action( 'init', 'simplon_taxonomies' );