<?php

// Includes
include 'theme-customizer/modules.php';
include 'theme-customizer/hooks.php';
include 'theme-customizer/helpers.php';
include 'theme-customizer/ajax.php';

function simplon_customize_register( $wp_customize ) {

	// Has to be at the top
	$wp_customize->register_panel_type( 'SIMPLON_WP_Customize_Panel' );

	// Removing Sections
	$wp_customize->remove_section( 'themes' );
	$wp_customize->remove_section( 'colors' );
	$wp_customize->remove_section( 'header_image' );
	$wp_customize->remove_section( 'theme_options' );
	$wp_customize->remove_section( 'custom_css' );
	$wp_customize->remove_section( 'static_front_page' );

	// The theme settings panel
	$wp_customize->add_panel(
		new SIMPLON_WP_Customize_Panel(
			$wp_customize,
			'simplon_settings',
			array(
				'title'    => __( 'simplon Settings', 'simplon' ),
				'priority' => 1,
			)
		)
	);

	// Special Pages Section
	$wp_customize->add_section( 'simplon_special_pages', array(
		'title' => __( 'Special Pages', 'simplon' ),
		'panel' => 'simplon_settings',
	));

	// Contact Page
	$wp_customize->add_setting( 'simplon_contact_page', array(
		'default'           => 0,
		'sanitize_callback' => 'absint',
		'transport'         => 'postMessage',
	));

	$wp_customize->add_control(
		new SIMPLON_Dropdown_Pages_Control(
			$wp_customize,
			'simplon_contact_page',
			array(
				'label'          => __( 'Contact Page', 'simplon' ),
				'section'        => 'simplon_special_pages',
				'settings'       => 'simplon_contact_page',
				'allow_addition' => true,
				// 'template'       => 'tpl-contact.php',
			)
		)
	);

	// News List Page
	$wp_customize->add_setting( 'simplon_news_page', array(
		'default'           => 0,
		'sanitize_callback' => 'absint',
		'transport'         => 'postMessage',
	));

	$wp_customize->add_control(
		new SIMPLON_Dropdown_Pages_Control(
			$wp_customize,
			'simplon_news_page',
			array(
				'label'          => __( 'News List Page', 'simplon' ),
				'section'        => 'simplon_special_pages',
				'settings'       => 'simplon_news_page',
				'allow_addition' => true,
				// 'template'       => 'tpl-news-list.php',
			)
		)
	);

	// background Section
	$wp_customize->add_section( 'simplon_change_background', array(
		'title' => __( 'Change background', 'simplon' ),
		'panel' => 'simplon_background',
	));

	$wp_customize->add_setting( 'simplon_background_color', array(
		'default'           => 0,
		'sanitize_callback' => 'absint',
		'transport'         => 'postMessage',
	));

	$wp_customize->add_control(
		new SIMPLON_Dropdown_Pages_Control(
			$wp_customize,
			'simplon_contact_page',
			array(
				'label'          => __( 'Contact Page', 'simplon' ),
				'section'        => 'simplon_special_pages',
				'settings'       => 'simplon_contact_page',
				'allow_addition' => true,
				// 'template'       => 'tpl-contact.php',
			)
		)
	);

	// // Language Switcher
	// $wp_customize->add_section( 'simplon_site_languages', array(
	// 	'title' => __( 'Languages', 'simplon' ),
	// 	'panel' => 'simplon_settings',
	// ));

	// // Address
	// $wp_customize->add_setting( 'simplon_language_switcher', array(
	// 	'default'   => 'yes',
	// 	'transport' => 'postMessage',
	// ));

	// $wp_customize->add_control(
	// 	'simplon_language_switcher',
	// 	array(
	// 		'label'    => __( 'Activate Language Switcher?', 'simplon' ),
	// 		'section'  => 'simplon_site_languages',
	// 		'settings' => 'simplon_language_switcher',
	// 		'type'     => 'radio',
	// 		'choices'  => array(
	// 			'yes' => __( 'Yes', 'simplon' ),
	// 			'no'  => __( 'No', 'simplon' ),
	// 		),
	// 	)
	// );

	// // Site Informations Section
	// $wp_customize->add_section( 'simplon_site_informations', array(
	// 	'title' => __( 'Site Informations', 'simplon' ),
	// 	'panel' => 'simplon_settings',
	// ));

	// // Address
	// $wp_customize->add_setting( 'simplon_address', array(
	// 	'default'           => '',
	// 	'transport'         => 'postMessage',
	// ));

	// $wp_customize->add_control(
	// 	new SIMPLON_Wysiwyg_Control(
	// 		$wp_customize,
	// 		'simplon_address',
	// 		array(
	// 			'label'           => __( 'Address', 'simplon' ),
	// 			'section'         => 'simplon_site_informations',
	// 			'settings'        => 'simplon_address',
	// 			// 'editor_settings' => $editor_settings_full,
	// 		)
	// 	)
	// );

}
add_action( 'customize_register', 'simplon_customize_register', 11 );

/**
 * Custom Post States
 *
 * @param   array    $post_states
 * @param   WP_Post  $post
 *
 * @return  array
 */
function simplon_custom_post_states ( $post_states, $post ) {

	if ( $post->post_type !== 'page' ) {
		return $post_states;
	}

	$contact_state = simplon_add_post_state ( $post, 'simplon_contact_page', __( 'Contact page', 'simplon' ) );

	if( $contact_state ) {
		$post_states[] = $contact_state;
	}

	$news_state = simplon_add_post_state ( $post, 'simplon_news_page', __( 'News List page', 'simplon' ) );

	if( $news_state ) {
		$post_states[] = $news_state;
	}

	return $post_states;

}
add_filter( 'display_post_states', 'simplon_custom_post_states', 1000, 2 );

/**
 * Add Lang to Post States
 *
 * @param   array    $post_states
 * @param   WP_Post  $post
 *
 * @return  array
 */
function simplon_custom_post_states_lang ( $post_states, $post ) {

	if ( $post->post_type !== 'page' || ! function_exists( 'pll_get_post_language' ) ) {
		return $post_states;
	}

	$post_lang = strtoupper( pll_get_post_language( (int) $post->ID ) );

	foreach ( $post_states as $key => $state ) {

		$post_states[ $key ] = sprintf( '%s (%s)', $state, $post_lang );

	}

	return $post_states;

}
// add_filter( 'display_post_states', 'simplon_custom_post_states_lang', 1001, 2 );
