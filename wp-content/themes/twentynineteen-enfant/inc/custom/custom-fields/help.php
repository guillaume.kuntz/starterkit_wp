<?php

acf_add_local_field_group ( array (
	'key'    => 'acf_aide',
	'title'  => 'Aide',
	'fields' => array (
		array (
			'key'   => 'field_5b446954c1234',
			'label' => 'Préambule',
			'name'  => '',
			'type'  => 'tab',
		),
		array (
			'key'     => 'field_5b44698423169',
			'label'   => 'Corps de Texte',
			'name'    => '',
			'type'    => 'message',
			'message' => '<h3>Corps de Texte</h3><span style="color: red;">/!\</span> Le corps de texte saisi dans la partie ci-dessus sera entre autres utilisé pour le référencement de votre site et doit être <span style="color: red;">impérativement</span> saisi.<br /><span style="color: red;">/!\</span> Il apparaîtra sur Google lors d\'une recherche ou lors d\'un partage sur les réseaux sociaux.',
		),
		array (
			'key'     => 'field_5b44698421234',
			'label'   => 'Image mise en avant',
			'name'    => '',
			'type'    => 'message',
			'message' => '<h3>Image mise en avant</h3><span style="color: red;">/!\</span> De même, l\'image mise en avant sera entre autres utilisée pour le référencement et le partage et devra donc être définie.',
		),
	),
	'location' => array (
		array (
			array (
				'param'    => 'post_type',
				'operator' => '==',
				'value'    => 'post',
				'order_no' => 0,
				'group_no' => 0,
			),
		),
		array (
			array (
				'param'    => 'post_type',
				'operator' => '==',
				'value'    => 'page',
				'order_no' => 0,
				'group_no' => 1,
			),
		),
	),
	'options' => array (
		'position'       => 'normal',
		'layout'         => 'default',
		'hide_on_screen' => array (),
	),
	'menu_order' => 0,
) );