<?php

/**
 * Add post types
 *
 * @return  void
 */
function simplon_post_types() {

	// register_post_type( 'hp-slider-item', array(
	// 	'public'          => true,
	// 	'labels'          => array(
	// 		'name'          => __ ( 'HP Slider Items', 'simplon' ),
	// 		'singular_name' => __ ( 'HP Slider Item', 'simplon' ),
	// 		'menu_name'     => __ ( 'HP Slider Items', 'simplon' ),
	// 	),
	// 	'capability_type' => 'post',
	// 	'supports'        => array( 'title', 'thumbnail' ),
	// 	'taxonomies'      => array( 'category' ), 
	// 	'menu_position'   => 4,
	// 	'menu_icon'       => 'dashicons-format-gallery',
	// 	'has_archive'     => false,
	// ) );

	register_post_type( 'project', array(
		'public'          => true,
		'labels'          => array(
			'name'          => __ ( 'Projects', 'simplon' ),
			'singular_name' => __ ( 'Project', 'simplon' ),
			'menu_name'     => __ ( 'Projects', 'simplon' ),
		),
		'capability_type' => 'post',
		'supports'        => array( 'title', 'editor', 'thumbnail', 'revisions', 'excerpt' ),
		'taxonomies'      => array( 'category' ), 
		'menu_position'   => 4,
		'menu_icon'       => 'dashicons-welcome-widgets-menus',
		'has_archive'     => true,
	) );
		
}
add_action('init', 'simplon_post_types');

/**
 * Translate Custom Post Types Slugs
 *
 * @param   array     $post_type_translated_slugs
 *
 * @return  array
 */
function simplon_translated_post_type_rewrite_slugs( $post_type_translated_slugs ) {

// 	return array(
// 		'team' => array(
// 			'fr' => array(
// 				'has_archive' => false,
// 				'rewrite'     => array(
// 					'slug' => 'equipe',
// 				),
// 			),
// 			'en' => array(
// 				'has_archive' => false,
// 				'rewrite'     => array(
// 					'slug' => 'team',
// 				),
// 			),
// 			'es' => array(
// 				'has_archive' => false,
// 				'rewrite'     => array(
// 					'slug' => 'equipo',
// 				),
// 			),
// 		),
// 		'project' => array(
// 			'fr' => array(
// 				'has_archive' => true,
// 				'rewrite'     => array(
// 					'slug' => 'projet',
// 				),
// 			),
// 			'en' => array(
// 				'has_archive' => true,
// 				'rewrite'     => array(
// 					'slug' => 'project',
// 				),
// 			),
// 			'es' => array(
// 				'has_archive' => true,
// 				'rewrite'     => array(
// 					'slug' => 'proyecto',
// 				),
// 			),
// 		),
// 	);

}
add_filter( 'pll_translated_post_type_rewrite_slugs', 'simplon_translated_post_type_rewrite_slugs' );
