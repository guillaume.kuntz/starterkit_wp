<?php

// Block direct requests
defined( 'ABSPATH' ) or die( '-1' );

echo $args['before_widget']; ?>

	<div class="widget">

		<a
            href="<?php echo esc_url($instance['link']); ?>"
            class="<?php echo esc_attr($instance['class']); ?>"
            target="<?php echo esc_attr($instance['target']); ?>"
        >
            <?php echo esc_html($instance['title']); ?>
        </a>

	</div>

<?php echo $args['after_widget'];
