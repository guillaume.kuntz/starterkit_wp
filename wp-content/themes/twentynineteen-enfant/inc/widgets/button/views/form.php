<?php

// Block direct requests
defined( 'ABSPATH' ) or die( '-1' );

?>

<p>
	<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'simplon' ); ?></label>
	<input id="<?php echo $this->get_field_id( 'title' ); ?>" class="widefat" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr($instance['title']); ?>" />
</p>

<p>
	<label for="<?php echo $this->get_field_id( 'link' ); ?>"><?php _e( 'Link:', 'simplon' ); ?></label>
	<input id="<?php echo $this->get_field_id( 'link' ); ?>" class="widefat" name="<?php echo $this->get_field_name( 'link' ); ?>" type="text" value="<?php echo esc_attr($instance['link']); ?>" />
</p>

<p>
	<label for="<?php echo $this->get_field_id( 'class' ); ?>"><?php _e( 'class:', 'simplon' ); ?></label>
	<input id="<?php echo $this->get_field_id( 'class' ); ?>" class="widefat" name="<?php echo $this->get_field_name( 'class' ); ?>" type="text" value="<?php echo esc_attr($instance['class']); ?>" />
</p>

<p>
	<label for="<?php echo $this->get_field_id( 'target' ); ?>"><?php _e( 'target:', 'simplon' ); ?></label>
	<input id="<?php echo $this->get_field_id( 'target' ); ?>" class="widefat" name="<?php echo $this->get_field_name( 'target' ); ?>" type="text" value="<?php echo esc_attr($instance['target']); ?>" />
</p>
