<?php

/**
* Simplon Button Widget
*/
class Simplon_Button_Widget extends Simplon_Widget {
	/**
	 * Construct the Widget object
	 */
	public function __construct() {
		$widget_ops = array(
			'classname'   => 'widget_simplon_button',
			'description' => __('Displays a button.', 'simplon'),
		);
		parent::__construct( 'simplon_button', __('Simplon Button', 'simplon'), $widget_ops, null );
	} // END public function __construct

	/**
	* Function that render widget
	*
	* @param mixed $args
	* @param mixed $instance
	*/
	public function widget( $args, $instance ) {
		$instance = wp_parse_args( $instance, $this->get_defaults() );

		include( $this->get_template_path( 'widget' ) );

	} // END public function widget

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		 // Rien à nettoyer
		return $new_instance;
	} // END public function update

	/**
	* Function that render form in Admin Widgets
	*
	* @param mixed $instance
	*/
	public function form( $instance ) {
		$instance = wp_parse_args( $instance, $this->get_defaults() );
		require( $this->get_template_path( 'form' ) );
	} // END public function form

	/**
	 * Default widget options
	 *
	 * @return array
	 */
	public function get_defaults() {
		return array(
			'title'       =>  __('Go to', 'simplon'),
			'link'        => '',
			'class'		  => '',
			'target'      => '',
		);
	} // END public function get_defaults

	/**
	* Auto register function
	*/
	static function register() {
		register_widget( 'Simplon_Button_Widget' );
	} // END public function register
} // END class Simplon_Button_Widget extends Simplon_Widget
