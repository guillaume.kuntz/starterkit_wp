<?php

// Custom Post Types, Custom Fields and Taxonomies
require_once( 'custom/post-types.php' );
require_once( 'custom/fields.php' );
require_once( 'custom/taxonomies.php' );

// Custom Admin
require_once( 'custom/theme-customizer.php' );
