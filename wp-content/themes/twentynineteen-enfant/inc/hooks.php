<?php

function simplon_enqueue_scripts()
{
    $version = wp_get_theme()->get('Version');
    $stylesheet_uri = get_stylesheet_directory_uri();

    wp_enqueue_style(
        'bootstrap4',
        'https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css',
        array(),
        $version
    );

    wp_enqueue_style(
        'wpm-twenty-nineteen-style',
        get_template_directory_uri() . '/style.css',
        array(),
        $version
    );
    wp_enqueue_style(
        'Nineteen-style-child',
        $stylesheet_uri . '/assets/css/style.css',
        array('wpm-twenty-nineteen-style', 'bootstrap4'),
        $version
    );

    wp_enqueue_script(
        'jquery-slim',
        'https://code.jquery.com/jquery-3.3.1.slim.min.js',
        array('jquery'),
        $version,
        false
    );
    wp_enqueue_script(
        'popper',
        'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js',
        array('jquery'),
        $version,
        false
    );
    wp_enqueue_script(
        'bootstrap',
        'https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js',
        array('jquery'),
        $version,
        false
    );
    wp_enqueue_script(
        'bxslider',
        $stylesheet_uri . '/assets/js/jquery.bxslider.min.js',
        array('jquery'),
        $version,
        false
    );
    wp_enqueue_script(
        'nineteen-child-script',
        $stylesheet_uri . '/assets/js/main.js',
        array('jquery-slim', 'popper', 'bootstrap', 'bxslider'),
        $version,
        false
    );
}
add_action('wp_enqueue_scripts', 'simplon_enqueue_scripts');

function simplon_theme_setup()
{
    add_image_size('homepage-slider', 1200, 400, true); // (cropped)
    add_image_size('homepage-competence', 300, 300, true); // (cropped)
    add_image_size('homepage-en_avant', 600, 600, true); // (cropped)

    // Text domain
    load_theme_textdomain( 'simplon', get_stylesheet_directory() . '/assets/lang' );
}
add_action('after_setup_theme', 'simplon_theme_setup');

//creation de la fonction pour supprimer le [...] et le remplacer par Continuer la lecture.
function simplon_excerpt_more($more)
{
    ob_start();
    ?>
        <a class="read-more" href="<?php echo esc_url(get_permalink(get_the_ID())); ?>">
            <?php esc_html_e("Read More", "simplon"); ?>
        </a>
    <?php
    $my_string = ob_get_clean();

    return $my_string;
}

add_filter('excerpt_more', 'simplon_excerpt_more');
add_filter('wp_calculate_image_srcset', '__return_false');

function simplon_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Sidebar bottom', 'simplon' ),
        'id' => 'sidebar-bottom',
        'description' => __( 'Zone de widget', 'simplon' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
	    'after_widget'  => '</div>',
	    'before_title'  => '<h2 class="widgettitle">',
	    'after_title'   => '</h2>',
    ) );
}

add_action( 'widgets_init', 'simplon_widgets_init' );
