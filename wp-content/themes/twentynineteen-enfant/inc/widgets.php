<?php

// button
require_once( dirname(__FILE__) . '/widgets/button/widget.php' );
add_action( 'widgets_init', array( 'Simplon_Button_Widget', 'register' ) );

/**
* Simplon BASE Widget
*/
class Simplon_Widget extends WP_Widget {
	/**
	 * Construct the Simplon_Widget object
	 */
	public function __construct( $id_base, $name, $widget_options = array(), $control_options = array() ) {
		parent::__construct( $id_base, $name, $widget_options, $control_options );
	} // END public function __construct

	/**
	 * Loads theme files
	 * Si vous voulez charger le template wp-content/themes/wpfpzero/inc/widgets/wpfpzero-content-widget/views/form.php
	 * Utilisez la fonction comme suit : $this->get_template_path( 'form' );
	 * Sans l'extension
	 * Cela retournera wp-content/themes/wpfpzero/inc/widgets/wpfpzero-content-widget/views/form.php
	 *
	 * @param   string      $template_slug      template file slug to search for
	 * @return  string                          template path
	 **/
	public function get_template_path( $template_slug ) {
		// whether or not .php was added
		$template = $template_slug . '.php';

		return 'views/' . $template;
	} // END public function get_template_path

	/**
	 * Loads Asset files
	 * In case you want to load a js (or css), simply use $this->get_asset_path( 'file_name', 'js' )
	 * In case you want to load an image, simply use $this->get_asset_path( 'file_name', 'images', $ext ), where $ext like "jpg" , "jpeg", "png", ... without the dot
	 *
	 * @param   string $asset   asset file to search for ;
	 * @param   string $type    type of the file (Could be "js", "css" or "images")
	 * @param   string $ext     the extension of the file if it's an image
	 * @return  string          Asset path
	 **/
	public function get_asset_path( $asset, $type, $ext = null ) {
		// whether or not .$type / .$ext was added
		$asset_slug = isset( $ext ) && !empty( $ext ) ? str_replace( '.' . $ext, '', $asset ) : str_replace( '.' . $type, '', $asset );
		$asset = isset( $ext ) && !empty( $ext ) ? $asset_slug . '.' . $ext : $asset_slug . '.' . $type;

		return get_template_directory_uri() . '/inc/widgets/assets/' . $type . '/' . $asset;
	} // END public function get_asset_path
} // END class Simplon_Widget extends WP_Widget
