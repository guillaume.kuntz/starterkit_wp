<?php if ( is_active_sidebar( 'sidebar-bottom' ) ) : ?>
	<div id="sidebar-bottom">
		<?php dynamic_sidebar( 'sidebar-bottom' ); ?>
	</div>
<?php endif; ?>
