<?php

//hooks utiliser dans hooks.php
    require_once 'inc/hooks.php';

//disable gutenberg
    require_once 'inc/gutenberg.php';

//shortcode
    require_once 'inc/shortcodes.php';

//widgets
    require_once 'inc/widgets.php';

//custom php
    require_once 'inc/custom.php';
