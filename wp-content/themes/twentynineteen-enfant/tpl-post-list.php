<?php
/*
* Template Name: News List
* Template Post Type: page
*/

get_header();
?>

<section id="primary" class="content-area">
    <main id="main" class="site-main">

        <?php

        /* Start the Loop */
        while ( have_posts() ) :
            the_post();

            get_template_part( 'template-parts/content/content', 'page' );

        endwhile; // End of the loop.
        ?>

        <div class="section-news">

        <?php

            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
            // The Query
                $args = [
                    'post_type' => ['post'],
                    'paged' => $paged,
                ];
                $news_query = new WP_Query( $args );

                // The Loop
                if ( $news_query->have_posts() ) :
                    global $wp_query;
                    $wp_query->max_num_pages = $news_query->max_num_pages;

                    while ( $news_query->have_posts() ) :
                        $news_query->the_post();

                        get_template_part( 'template-parts/list/content', 'bloc' );

                    endwhile;

                    the_posts_pagination ( array(
                        'mid_size'           => 2,
                        'end_size'           => 0,
                        'prev_text'          => '<',
                        'next_text'          => '>',
                        'before_page_number' => '<span class="meta-nav screen-reader-text">' . esc_html__( 'Page', 'simplon' ) . ' </span>',
                    ) );

                    /* Restore original Post Data */
                    wp_reset_postdata();
                    else :
                        // no posts found
                    endif;

            ?>

            </div>

        </main><!-- #main -->
    </section><!-- #primary -->

    <?php
    get_footer();
