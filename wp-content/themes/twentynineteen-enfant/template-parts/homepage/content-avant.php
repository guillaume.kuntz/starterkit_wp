<h1 id="avant">Mises-en-avant</h1>

<section class="section-avant">

    <?php

    // check if the repeater field has rows of data
    if (have_rows('en_avant')) :

    // loop through the rows of data
        while (have_rows('en_avant')) : the_row();

            // display a sub field value
            $texte = get_sub_field('zone_de_texte');
            $img = get_sub_field('image_en_avant');
            //print_r($img);

            ?>
            <div class="en_avant">
                <div class="avant col-md-6">
                    <?php if (!empty($img)) :?>
                        <img width="<?php echo $img['sizes']['homepage-en_avant-width']; ?>" height="<?php echo $img['sizes']['homepage-en_avant-height']; ?>" src="<?php echo $img['sizes']['homepage-en_avant']; ?>" alt="<?php echo $img['alt'] ?>" />
                    <?php endif; ?>
                </div>
                <div class="avant_texte col-md-6">
                    <?php if (!empty($texte)) : ?>
                        <p><?php echo $texte; ?></p>
                    <?php endif;?>
                </div>
            </div>

            <?php
        endwhile;
        ?>

    <?php
    else :

        // no rows found

    endif;
?>
</section>
