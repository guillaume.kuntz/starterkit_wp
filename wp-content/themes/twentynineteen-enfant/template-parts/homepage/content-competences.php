<h1 id="competences">Compétences</h1>

<section class="section-competences">

    <?php

    // check if the repeater field has rows of data
    if (have_rows('competence')) :

    // loop through the rows of data
        while (have_rows('competence')) : the_row();

            // display a sub field value
            $texte = get_sub_field('texte');
            $img = get_sub_field('image');
            //print_r($img);

            ?>

            <div class="col-md-4 col-sm-12">
                <div class="competences">
                    <?php if (!empty($img)) :?>
                        <img width="<?php echo $img['sizes']['homepage-competence-width']; ?>" height="<?php echo $img['sizes']['homepage-competence-height']; ?>" src="<?php echo $img['sizes']['homepage-competence']; ?>" alt="<?php echo $img['alt'] ?>" />
                    <?php endif; ?>
                    <?php if (!empty($texte)) : ?>
                        <p><?php echo $texte; ?></p>
                    <?php endif;?>
                </div>
            </div>

            <?php
        endwhile;
        ?>

    <?php
    else :

        // no rows found

    endif;

    ?>
</section>
