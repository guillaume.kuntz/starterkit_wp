<!-- les template appeler dan sma page acceuil -->
<section class="section-homepage">
    <?php

    //template slider
    get_template_part( 'template-parts/homepage/content', 'slider' );

    //template compétences
    get_template_part( 'template-parts/homepage/content', 'competences' );

    //template avant
    get_template_part( 'template-parts/homepage/content', 'avant' );

    //template news
    get_template_part( 'template-parts/list/loop', 'news' );
    ?>
</section>
