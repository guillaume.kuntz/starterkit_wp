<h1 id="slider">Slider</h1>

<section class="section-slider">

    <?php

    // check if the repeater field has rows of data
    if (have_rows('slider')) :
        ?>
        <div class="homepage-slider">

            <?php

            // loop through the rows of data
            while (have_rows('slider')) : the_row();

            // display a sub field value
            $titre = get_sub_field('titre');
            $sous_titre = get_sub_field('sous-titre');
            $img = get_sub_field('image');

            // print_r($img);

            ?>
            <div>
                <?php if (!empty($titre)) : ?>
                    <h1 class="titre"><?php echo $titre; ?></h1>
                <?php endif; ?>

                <?php if (!empty($sous_titre)) : ?>
                    <h3><?php echo $sous_titre; ?></h3>
                <?php endif; ?>

                <?php if (!empty($img)) : ?>
                    <img width="<?php echo $img['sizes']['homepage-slider-width']; ?>" height="<?php echo $img['sizes']['homepage-slider-height']; ?>" src="<?php echo $img['sizes']['homepage-slider']; ?>" alt="<?php echo $img['alt'] ?>" />
                <?php endif; ?>
            </div>

            <?php
        endwhile;
        ?>
    </div><!-- homepage-slider -->

    <?php
    else :

        // no rows found

    endif;
    ?>
</section>
