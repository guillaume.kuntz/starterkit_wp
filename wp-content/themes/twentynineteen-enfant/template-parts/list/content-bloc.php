<article class="article-bloc">

    <div class="titre">
        <span>
            <?php
                the_title();
            ?>
        </span>
    </div>

    <div class="news">
        <?php
            the_post_thumbnail('thumbnail' , [
                'class' => 'post-thumbnail',
                // 'alt' => ''
            ]);
            the_content();
            the_excerpt();
        ?>
    </div>

</article>
