<h1  id="news">News</h1>

<section class="section-news">


    <?php

        // The Query
        $args = [
            'post_type' => ['post'],
            'posts_per_page' => 3 ,
        ];
        $new_query = new WP_Query( $args );

        // The Loop
        if ( $new_query->have_posts() ) :

        	while ( $new_query->have_posts() ) :
        		$new_query->the_post();

                get_template_part( 'template-parts/list/content', 'bloc' );

        	endwhile;

        	/* Restore original Post Data */
        	wp_reset_postdata();
        else :
        	// no posts found
        endif;

    ?>

</section>
